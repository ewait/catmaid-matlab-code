%check CellTracking notebook in noteshelf at 08/27/2013
%first call errFreeLineage.m to obtain branchCellErr
function numOpsPerTimePoint  = numberOfEditingOperationsPerTimePoint( branchInfo, branchCellErr)

numOpsPerTimePoint = cell( max(branchInfo(:,2)),1);

N = size( branchInfo,1);
for kk = 1:N
   iniTM = branchInfo(kk,1);
   endTM = branchInfo(kk,2);
   
   tErr = branchCellErr{kk};
   if( branchInfo(kk,3) ~= length(tErr) )
       error 'Quantities should match'
   end
   
   tt = [iniTM tErr endTM];
   qq = diff(tt);
   
   %find blocks where errors are correlated
   pp = find( qq >= 3 );%these are considered initial points to start fixing
   %special case for first 4 time points
   if( qq(1) < 3 )
       pp = [1 pp];
   end
   
   for ii = 1:length(pp)-1 %last element is the end of teh branch
      if( qq(pp(ii)+1) < 3 )%we have correlated errors
          count = 2;
          while ( qq(pp(ii)+count) < 3)
             count = count + 1; 
          end
          tAux = tt( pp(ii) + 1 ) + 1;
          for jj = 1:count
            numOpsPerTimePoint{tAux + jj - 1} = [ numOpsPerTimePoint{tAux + jj - 1} (3+count)/count ]; 
          end
          
      else%independent errors
          tAux = tt( pp(ii) + 1 ) + 1;%because time points start in zero indexing
          numOpsPerTimePoint{tAux} = [ numOpsPerTimePoint{tAux} 4 ]; 
      end
   end
   
end



