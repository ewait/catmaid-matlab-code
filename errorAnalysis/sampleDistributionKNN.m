%calculates convex hull and then returns distance of each point to that
%convex hull

%based on code form Dan Mossing
function knnDistribution = sampleDistributionKNN(xyz,xyzFullEmbryoCoverage, numLocalNeigh)


%create kd-tree
 ns = createns(xyzFullEmbryoCoverage);
 
%calculate nearest neighbor for each point
[~, knnDist] = knnsearch(ns,xyzFullEmbryoCoverage,'k',2);
knnDist = knnDist(:,2);


%for each xyz point, calculate knn Distribution by averaging knn 20 closest points 
knnDistribution = zeros(size(xyz,1),1);

[idx,~] = knnsearch(ns,xyz,'k',numLocalNeigh);
for ii = 1:length(knnDistribution)
    knnDistribution(ii) = median(knnDist(idx(ii,:)));    
end




