%trackingM contains id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
function branchLength = branchLength(trackingM)

N = size(trackingM,1);
nodeIdMap = containers.Map(trackingM(:,1),[1:N]);



%find num children for each node
numCh = zeros(N,1);

for kk = 1: N
    par = trackingM(kk,7);
    
    if( par < 0 || isKey(nodeIdMap, par)== false )
        continue;
    end
    aux = nodeIdMap( par );
    numCh ( aux ) = numCh( aux ) + 1;
end

%for each leaf node( numCh == 0 ) backtrack to find branch length
pos = find( numCh == 0 );

branchLength = zeros(length(pos),1);
for kk = 1: length(pos)
    count = 1;% we already have th eleave
    
    par = trackingM( pos(kk), 7 );
    while( 1 )
        if( par < 0 || isKey(nodeIdMap, par)== false )
            break;
        end
        
        parPos = nodeIdMap( par );
        if( numCh( parPos ) == 2 )% found a division
            break;
        end
        
        %update elements
        count = count + 1;
        par = trackingM( parPos, 7 );
    end
    
    branchLength(kk) = count;
end

