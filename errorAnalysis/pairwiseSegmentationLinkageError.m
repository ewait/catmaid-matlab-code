%INPUT:

%imgPatternFilename: optional if we want to generate thumbnails to explore
%the mistakes and correct

%trackingMatrixGT: Nx10 array with GT from database
%trackingMatrixErrorCode: Nx1 array. 2->segm. & link. correct; 1->segm.
%correct; 0->incorret
function [timeT,numSegmentationTotal, numSegmentationCorrect, numLinkageTotal, numLinkageCorrect,...
           trackingMatrixGT, trackingMatrixErrorCode,...
           distErr, typeErr, markedErr, numTotalTGMMpts, numTotalConfidence0pts, numTotalCheckedpts ]...
           = pairwiseSegmentationLinkageError(projectName, basenameXMLGMM, imgPatternFilename, generateThumbnails)

disp('TODO: count cell division errors')
disp('TODO: USE MAHLANABIS DISTANCE TO CALCULATE ASSIGNMENT') %(x-mu)' * W * (x-mu) <= maxDistanceSegmentationError

%fixed parameters
maxDistanceSegmentationError = 10.0; %max distance to consider that two points are the same between ground truth and automatic reconstruction
thumbnailSize = 50; %radius in pixels (so size is 1 + 2 * thumbnailSize)

%main parameters
if(nargin < 1)
    projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID
    basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_6_6_21_28_43_dataset12-08-28_TM0-1000_neuroblastAnnotation_simpleLogicalRules_noStructuralLearning\XML_finalResult_lht\GMEMfinalResult_frame';
    imgPatternFilename = 'G:/12-08-28/Results/TimeFused.Blending/Dme_E1_His2ARFP.TM?????_timeFused_blending/SPC0_CM0_CM1_CHN00_CHN01.fusedStack_?????.jp2';
    generateThumbnails = [-1 -1];
end


%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);


if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 '];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

treenodeIdMap = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

rmpath( pathAdd );


%Database connections, cursors, and resultset objects remain open until you close them using the close function. 
%Always close a cursor, connection, or resultset when you finish using it. Close a cursor before closing the connection used for that cursor.
close(connDB);


%load all the xml solutions (so we can follow lineages to correlate errors)
%%
%{
tic;
minT = min( trackingMatrixGT( : , 8 ) );
maxT = max( trackingMatrixGT( : , 8 ) );
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
objCell = cell(maxT+1,1);
for frame = minT : maxT
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];
    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        error 'XML solution does not exist'
    end
    objCell{frame + 1} = readXMLmixtureGaussians(TGMMfilename );
    
end
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
toc;
%}

%----------------------------------------------------------------------
%%
%start processing each timepoint
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'

minT = min( trackingMatrixGT( : , 8 ) );
maxT = max( trackingMatrixGT( : , 8 ) );

timeT = [minT:maxT]';
numSegmentationTotal = zeros(maxT - minT + 1, 1);
numSegmentationCorrect = numSegmentationTotal;
numLinkageTotal = numSegmentationTotal;
numLinkageCorrect = numSegmentationTotal;
trackingMatrixErrorCode = zeros( size(trackingMatrixGT,1), 1);

%for debugging purposes to analyze errors based on change sin the algorithm
numSegmentationCorrectScore = cell(maxT - minT + 1, 1);
numSegmentationWrongScore = numSegmentationCorrectScore;

tic;
distErr = -ones( size(trackingMatrixGT,1), 2);
typeErr = -ones( size(trackingMatrixGT,1), 1);%-1->indicates no error
markedErr = -ones( size(trackingMatrixGT,1), 1);

numTotalTGMMpts = zeros( maxT+1,1);
numTotalCheckedpts = numTotalTGMMpts;%to calculate the percentage of the data we need to check
numTotalConfidence0pts = numTotalTGMMpts;%at the end, these two variables should agree

if( generateThumbnails(1) > 0 )
    thumbnailDir = ['E:/2013-NatMethods-TGMM/thumbnails/' datestr(now,30) '/'];
    mkdir( thumbnailDir );
end

%%
%disp('WARNING: testing short par loop!!!!!!');
for frame = minT : maxT
    
    
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        break
    end
    obj = readXMLmixtureGaussians(TGMMfilename );
    
    %obj = objCell{frame+1};
        
    GT = trackingMatrixGT(trackingMatrixGT(:,8) == frame, : );
    numSegmentationTotal(frame - minT + 1) = size(GT,1);
    
    trackingMatrixErrorCodeAux = zeros(size(GT,1),1);
    
    
    %load image
    imgFilename = recoverFilenameFromPattern(imgPatternFilename, frame);
    [pathstr, name, ext] = fileparts(imgFilename) ;
    
    if( strcmp(ext,'.jp2') == 1 )
        im = readJPEG2000stack(imgFilename, 8 );
    else
        im = readTIFFstack(imgFilename );
    end
    %get interpolated Z between stacks (manual clicking can be off by +-2 slices
    [ZinterpPixels, ~ ] = interpolateZ(im,GT(:,3:5), scale, 2);
    GT(:,5) = ZinterpPixels * scale(3);
    trackingMatrixGT(trackingMatrixGT(:,8) == frame, 5 ) = GT(:,5);%for linkage error
         
    
    %parse xyz points from automatic tracking
    xyz = zeros(length(obj),3);
    parId = zeros(length(obj),1);
    score = zeros(length(obj),1);
    for kk = 1: size(xyz,1)
        xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
        parId(kk) = obj(kk).parent + 1;%matlab index
        score(kk) = obj(kk).splitScore;
        if( obj(kk).id ~= kk-1 )
            error 'GMM Index is not sequential!'
        end
    end
    for kk = 1:3
        xyz(:,kk) = xyz(:,kk) * scale(kk);
    end
    numTotalTGMMpts(frame+1) = length(obj);
    numTotalConfidence0pts(frame+1) = sum( score == 0 );
    
    %count segmentation errors
    [idx, dist] = knnsearch(xyz, GT(:,3:5),'k',2);%dist has the size of GT
    [idxErrType, distErrType] = knnsearch(xyz, GT(:,3:5),'k',5);%to calculate types of errors
    pos = find( dist(:,1) < maxDistanceSegmentationError & (dist(:,2)./ dist(:,1)) > 1.5 );
    numSegmentationCorrect(frame-minT + 1) = length(pos);%second possible match is far away
    trackingMatrixErrorCodeAux( pos ) = 1;
    if( frame == minT )
        trackingMatrixErrorCodeAux( pos ) = 2;%linkage is automatically OK as well
    end
    %TODO: IMPLEMENT MAHALANBIS DISTANCE INSTEAD OF EUCLIDEAN (STILL USING KNN TO PRUNE CANDIDATES);
    %NOTE: WE WOULD NEED precision matrix for GT, since using precision
    %matrix from TGMM does guarantee that the distance is correct since GT
    %is not exhaustive (one big blob could seem like correct segmentation)
    
    
    
    if( frame >= generateThumbnails(1) &&  frame <= generateThumbnails(2) )
        
        
        %load supervoxels if file exists
        SVfilename = [basenameXMLGMM num2str(frame,'%.4d') '.svb'];
        if( exist(SVfilename,'file') == 0 )%we do not have more tracking information
            drawSV = false;
        else
            drawSV = true;
            set(0,'defaultAxesFontName', 'Arial')
            set(0,'defaultTextFontName', 'Arial')
            svStruct = readListSupervoxelsFromBinaryFile( SVfilename );
        end
        frameDir = [thumbnailDir 'TM' num2str(frame,'%.4d') '/'];
        mkdir( frameDir );
        
        
        
        %====================================================================
        %save thumbnails if requested for correct segmentation                                      
        %examples of correct segmentation
        for aa = 1:length(pos)
            pp = xyz(idx(pos(aa),1),[2 1 3]) ./ scale;%xy flip and scale normalized
            thumbnailBasename = ['Segmentation_TM_' num2str(frame,'%.4d') '_correct_' num2str(GT(pos(aa),1),'%d')];
            if( drawSV == false )
                svPixelIdxList = [];
            else
                %generate supervoxel
                svPixelIdxList = svStruct{ obj( idx(pos(aa),1) ).svIdx(1) + 1};
                for gg = 2:length(obj( idx(pos(aa),1) ).svIdx)
                    svPixelIdxList = [ svPixelIdxList ;svStruct{ obj( idx(pos(aa),1) ).svIdx(gg) + 1} ];
                end
            end
            %generate figure
            thumbnailCell = cell(3,1);
            hSVcell = cell(3,1);
            suffix = {'','_XZ','_YZ'};
            
            [thumbnailCell{1}, hSVcell{1}, thumbnailCell{2}, hSVcell{2},thumbnailCell{3} , hSVcell{3}] = generateThumbnail(pp, im, thumbnailSize, scale(3) / scale(1), svPixelIdxList );
            thumbnailBasenameSV = [thumbnailBasename '_sv'];
            for gg = 1:3
                %save thumbnail
                imwrite(thumbnailCell{gg}, [frameDir thumbnailBasename suffix{gg} '.tif']);
                %save contour
                if( isempty( hSVcell{gg} ) == false )
                    printeps( hSVcell{gg}, [frameDir thumbnailBasenameSV suffix{gg}] );
                    saveas(hSVcell{gg},[frameDir thumbnailBasenameSV suffix{gg} '.tif']);%to visualize quicickly in explorer
                    close(hSVcell{gg});
                end
            end
        end
        
        
        %end for correct segmentation
        %=========================================================
        
    
        
        
        %====================================================================
        %save thumbnails if requested for incorrect segmentation
        posN = [1:size(idx,1)];
        posN(pos) = [];
        %examples of incorrect segmentation
        for aa = 1:length(posN)
            
            if ( dist(posN(aa),1) > maxDistanceSegmentationError )
                segSuffix = 'missing';
                ppGT = GT(posN(aa),[4 3 5])./scale;
                
                
                if( drawSV == false )
                    svPixelIdxList = [];
                else
                    %generate supervoxel
                    svPixelIdxList = svStruct{ obj( idx(posN(aa),1) ).svIdx(1) + 1};
                    for gg = 2:length(obj( idx(posN(aa),1) ).svIdx)
                        svPixelIdxList = [ svPixelIdxList ;svStruct{ obj( idx(posN(aa),1) ).svIdx(gg) + 1} ];
                    end
                end
                
                %generate figure
                thumbnailCell = cell(3,1);
                hSVcell = cell(3,1);
                suffix = {'','_XZ','_YZ'};
                
                [thumbnailCell{1}, hSVcell{1}, thumbnailCell{2}, hSVcell{2},thumbnailCell{3} , hSVcell{3}] = generateThumbnail(ppGT, im, thumbnailSize, scale(3) / scale(1), svPixelIdxList );
                
                for gg =1:3
                    %set to black the two closest points point
                    for hh = 1:1 %for presentations, it is better only nearest. For debugging errros, better hh =1:2
                        pp = xyz(idx(posN(aa),hh),[2 1 3]) ./ scale;
                        offsetXY = round((pp - ppGT)./scale);
                        oAux = [1:3];
                        oAux(4-gg) = [];
                        offsetXY = round(offsetXY(oAux) + ( size( thumbnailCell{gg} ) - 1 ) / 2 + 1);%offset to center of the thumbnail
                        thumbnailCell{gg}( offsetXY(1), offsetXY(2) ) = 0;
                    end
                    
                end
            else%another point too close: oversegmentation
                segSuffix = 'oversegmentation';
                pp = xyz(idx(posN(aa),1),[2 1 3]) ./ scale;%xy flip and scale normalized
                
                
                if( drawSV == false )
                    svPixelIdxList = [];
                else
                    %generate supervoxel
                    svPixelIdxList = svStruct{ obj( idx(posN(aa),1) ).svIdx(1) + 1};
                    for gg = 2:length(obj( idx(posN(aa),1) ).svIdx)
                        svPixelIdxList = [ svPixelIdxList ;svStruct{ obj( idx(posN(aa),1) ).svIdx(gg) + 1} ];
                    end
                end
                
                %generate figure
                thumbnailCell = cell(3,1);
                hSVcell = cell(3,1);
                suffix = {'','_XZ','_YZ'};
                
                [thumbnailCell{1}, hSVcell{1}, thumbnailCell{2}, hSVcell{2},thumbnailCell{3} , hSVcell{3}] = generateThumbnail(pp, im, thumbnailSize, scale(3) / scale(1), svPixelIdxList );
                
                
                %set to black the other point
                pp2 = xyz(idx(posN(aa),2),[2 1 3]) ./ scale;
                for gg =1:3
                    offsetXY = round( (pp2 - pp)./scale );
                    oAux = [1:3];
                    oAux(4-gg) = [];
                    offsetXY = offsetXY(oAux) + ( size( thumbnailCell{gg} ) - 1 ) / 2 + 1;%offset to center of the thumbnail
                    thumbnailCell{gg}( offsetXY(1), offsetXY(2) ) = 0;
                end
            end
            
            thumbnailBasename = ['Segmentation_TM_' num2str(frame,'%.4d') '_incorrect-' segSuffix '_'  num2str(GT(posN(aa),1),'%d')];
            
            thumbnailBasenameSV = [thumbnailBasename '_sv'];
            for gg = 1:3
                %save thumbnail
                imwrite(thumbnailCell{gg}, [frameDir thumbnailBasename suffix{gg} '.tif']);
                %save contour
                if( isempty( hSVcell{gg} ) == false )
                    printeps( hSVcell{gg}, [frameDir thumbnailBasenameSV suffix{gg}] );
                    saveas(hSVcell{gg},[frameDir thumbnailBasenameSV suffix{gg} '.tif']);%to visualize quicickly in explorer
                    close(hSVcell{gg});
                end
            end
        end
        %end for incorrect segmentation thumbnails
        %==========================================
        
    end 
    
    
    
    
    %save info on errors
    posN = [1:size(idx,1)];
    posN(pos) = [];
    for aa = 1:length(posN)
       distErr( treenodeIdMap( GT(posN(aa),1)),: ) = dist(posN(aa),:); 
    end
    
    typeErrAux = zeros(length(posN),1);%0->unknown; 1->cell death; 2->cell division; 3->large displacement
    markedErrAux = typeErrAux;
    
    numSegmentationCorrectScore{frame-minT + 1} = score( idx(pos,1) );
    numSegmentationWrongScore{frame-minT + 1} = score( idx(posN,1) );
    
    %check if errors havea nearby cell division or death or large
    %displacements
    if( frame > minT )
        %calculate displacement distribution to find out large
        %displacements
        GTold = trackingMatrixGT(trackingMatrixGT(:,8) == frame-1, : );
        [~,dispD] = knnsearch(GT(:,3:5), GTold(:,3:5),'k',1);
        thrD = median(dispD) + 3 *  mad(dispD) * 1.253 ;
        [idxErrTypeDeath, distErrTypeDeath] = knnsearch(xyzOld, GT(:,3:5),'k',5);%to calculate errors based on death
        
        %calculate global stats on TGMM on how many time points we would have to check
        %cell death
        auxParId = parId(parId>0);
        aux = size(xyzOld,1) - length(unique(auxParId));
        numTotalCheckedpts( frame ) = numTotalCheckedpts( frame ) + aux;
        %cell division        
        aux = length(auxParId) - length(unique(auxParId));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + aux;
        %large displacement                
        aux = sqrt(sum( ((xyz( parId > 0, : ) - xyzOld( auxParId , :)) ).^2,2));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + sum(aux > thrD);
        
        for kk = 1:length(posN)
            
            pp = posN(kk);
            %check if neighbors (in time and space) have a division
            %TODO: expand time search to +-2 time points (right now only to
            %-1)
            
            %check if neighbors in previous time point died            
            for aa = 1:size(idxErrTypeDeath,2)
                auxParId = idxErrTypeDeath( pp, aa);%this is ID in xyzOld
                if( sum( parId == auxParId ) == 0 )
                    typeErrAux(kk) = 1;%cell death
                end
                if( scoreOld( auxParId ) == 0 )
                    markedErrAux(kk) = 1;%it was marked in CATMAID
                end
            end                        
            
            if( typeErrAux(kk) == 0 )%not assigned yet
                for aa = 1:size(idxErrType,2)
                    
                    auxParId = idxErrType( pp, aa);
                    if( score( auxParId ) == 0 )
                        markedErrAux(kk) = 1;%it was marked in CATMAID
                    end
                    
                    if (sum( parId == parId( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        break;
                    end
                    %check if it is large displacement
                    if( parId( auxParId ) > 0 )
                        if( norm (xyz( auxParId ,: ) - xyzOld( parId( auxParId ), :)) > thrD )
                            typeErrAux(kk) = 3;%large displacement
                        end
                    end
                    
                    %previous time point
                    auxParId = parId( idxErrType( pp, aa) );
                    if( scoreOld( auxParId) == 0 )
                        markedErrAux(kk) = 1;
                    end
                    if (sum( parIdOld == parIdOld( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        break;
                    end
                    
                end
            end                                                
        end
    end
    
    for aa = 1:length(posN)
       typeErr( treenodeIdMap( GT(posN(aa),1))) = typeErrAux(aa); 
       markedErr( treenodeIdMap( GT(posN(aa),1))) = markedErrAux(aa); 
    end
    
    %count linkage errors
    if( frame > minT )
        numLinkageTotal(frame-minT + 1) = length(pos);
        for kk = 1:length(pos)
            nodeId = GT(pos(kk),1);
            nodeIdPar = trackingMatrixGT( treenodeIdMap(nodeId), 7 );
            if( isKey( treenodeIdMap, nodeIdPar ) == false )%we do not have parent in ground truth
                numLinkageTotal(frame-minT + 1) = numLinkageTotal(frame-minT + 1) - 1;
                continue;
            end
            xyzParGT = trackingMatrixGT( treenodeIdMap(nodeIdPar), 3:5);
            
            %find xyz for parent in automatic tracking
            parIdM = parId( idx(pos(kk),1) );
            if( parIdM <= 0 )
                continue;%mistake
            end
            xyzPar = xyzOld( parIdM, :);
            
            %only count this if parIdM was part of the correct segmentation
            if( sum(parIdM == idxOld( posOld,1 ) ) == 0 )
                numLinkageTotal(frame-minT + 1) = numLinkageTotal(frame-minT + 1) - 1;
                continue;
            end
            
            isCorrectSuffix = 'incorrect';
            if( norm([xyzPar-xyzParGT] ) < maxDistanceSegmentationError )
                numLinkageCorrect(frame-minT+1) = numLinkageCorrect(frame-minT+1) + 1;
                trackingMatrixErrorCodeAux( pos(kk) ) = 2;
                isCorrectSuffix = 'correct';
            end
            
            %=====================================================================
            %save thumbnail if needed for correct/incorrect linkage            
            if( frame > generateThumbnails(1) &&  frame <= generateThumbnails(2) )%here sis strict > so we have information from previous time point
                %examples of correct / incorrect linkage
                pp = xyzPar([2 1 3]) ./ scale;%xy flip and scale normalized
                
                
                if( drawSV == false )
                    svPixelIdxList = [];
                else
                    %generate supervoxel
                    svPixelIdxList = svStructOld { objOld( parIdM ).svIdx(1) + 1};
                    for gg = 2:length(objOld( parIdM ).svIdx)
                        svPixelIdxList = [ svPixelIdxList ;svStructOld{ objOld( parIdM ).svIdx(gg) + 1} ];
                    end
                end
                
                %generate figure
                thumbnailCell = cell(3,1);
                hSVcell = cell(3,1);
                suffix = {'','_XZ','_YZ'};
                
                [thumbnailCell{1}, hSVcell{1}, thumbnailCell{2}, hSVcell{2},thumbnailCell{3} , hSVcell{3}] = generateThumbnail(pp, imOld, thumbnailSize, scale(3) / scale(1), svPixelIdxList );
                                                
                thumbnailBasename = ['Linkage_TM_' num2str(frame,'%.4d') '_' isCorrectSuffix '_' num2str(nodeIdPar,'%d') '_' num2str(nodeId,'%d') '_A'];
                thumbnailBasenameSV = [thumbnailBasename '_sv'];
                for gg = 1:3
                    %save thumbnail
                    imwrite(thumbnailCell{gg}, [frameDir thumbnailBasename suffix{gg} '.tif']);
                    %save contour
                    if( isempty( hSVcell{gg} ) == false )
                        printeps( hSVcell{gg}, [frameDir thumbnailBasenameSV suffix{gg}] );
                        saveas(hSVcell{gg},[frameDir thumbnailBasenameSV suffix{gg} '.tif']);%to visualize quicickly in explorer
                        close(hSVcell{gg});
                    end
                end
                
         
                %new set of points
                pp = xyz(idx(pos(kk),1),[2 1 3]) ./ scale;
                
                if( drawSV == false )
                    svPixelIdxList = [];
                else
                    %generate supervoxel
                    svPixelIdxList = svStruct{ obj( idx(pos(kk),1) ).svIdx(1) + 1};
                    for gg = 2:length(obj( idx(pos(kk),1) ).svIdx)
                        svPixelIdxList = [ svPixelIdxList ;svStruct{ obj( idx(pos(kk),1) ).svIdx(gg) + 1} ];
                    end
                end
                
                %generate figure
                thumbnailCell = cell(3,1);
                hSVcell = cell(3,1);
                suffix = {'','_XZ','_YZ'};
                
                [thumbnailCell{1}, hSVcell{1}, thumbnailCell{2}, hSVcell{2},thumbnailCell{3} , hSVcell{3}] = generateThumbnail(pp, imOld, thumbnailSize, scale(3) / scale(1), svPixelIdxList );
                
                %save figures
                thumbnailBasename = ['Linkage_TM_' num2str(frame,'%.4d') '_' isCorrectSuffix '_' num2str(nodeIdPar,'%d') '_' num2str(nodeId,'%d') '_B'];
                thumbnailBasenameSV = [thumbnailBasename '_sv'];
                for gg = 1:3
                    %save thumbnail
                    imwrite(thumbnailCell{gg}, [frameDir thumbnailBasename suffix{gg} '.tif']);
                    %save contour
                    if( isempty( hSVcell{gg} ) == false )
                        printeps( hSVcell{gg}, [frameDir thumbnailBasenameSV suffix{gg}] );
                        saveas(hSVcell{gg},[frameDir thumbnailBasenameSV suffix{gg} '.tif']);%to visualize quicickly in explorer
                        close(hSVcell{gg});
                    end
                end
            end %end of correct/incorrect linkage thumbnails
            %=======================================================
            
        end
    end
    
    
    trackingMatrixErrorCode(trackingMatrixGT(:,8) == frame ) = trackingMatrixErrorCodeAux;
    
    idxOld = idx;
    idxErrTypeOld = idxErrType;
    distErrTypeOld = distErrType;
    posOld = pos;
    distOld = dist;
    xyzOld = xyz;
    parIdOld = parId;
    scoreOld = score;
    GTold = GT;
    imOld = im;
    objOld = obj;
    if( frame >= generateThumbnails(1) &&  frame <= generateThumbnails(2) )
        svStructOld = svStruct;
    end
    
    if (mod(frame,50) == 0)
        tt = toc;
        disp(['Finished frame ' num2str(frame) '/' num2str(maxT) ' in ' num2str(tt) ' secs']);
        display(['Total of ' num2str(sum(numSegmentationTotal)) ' nodes in GT. Segmentation accuracy is ' num2str( sum(numSegmentationCorrect) / sum(numSegmentationTotal) ) '.Linkage accuracy is ' num2str( sum(numLinkageCorrect) / sum(numLinkageTotal) )]);
        tic;
    end
end %end for frame:minT:maxT
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'


%%
%-------------------------------------------------------
%map children in order to traverse forward
childrenIdMap = -ones( size(trackingMatrixGT,1) , 2);%  treenodeIdMap ( trackingMatrixGT( childrenIdMap(i,1), 7 ) ) = i
for kk = 1:size(trackingMatrixGT,1)
    parId = trackingMatrixGT(kk,7);
    if( isKey( treenodeIdMap, parId ) == true )
        parIdx = treenodeIdMap( parId );
        if( childrenIdMap(parIdx,1) < 0 )
            childrenIdMap(parIdx,1) = kk;
        else
            childrenIdMap(parIdx,2) = kk;
        end
    end
end
%%
%-----------------correlate marked errors in time(since they tend to be correlated---------------------------
pos = find( typeErr > 0 );
nOrig = length(pos);
for kk = 1:length(pos)
    %propagate backwards
    gapSize = 0;
    nodeIdx = treenodeIdMap( trackingMatrixGT(pos(kk),1) );
    typeErrOrig = typeErr( pos(kk) );
    while(gapSize < 3)%we still consider errors to be correlated
        parId = trackingMatrixGT( nodeIdx, 7);
        if( isKey(treenodeIdMap,parId) == true )
            parIdx = treenodeIdMap( parId );
            if( typeErr( parIdx ) < 0 )%this was not an error
                gapSize = gapSize + 1;
            else
                gapSize = 0;
                if( typeErr( parIdx ) == 0 )
                    typeErr( parIdx ) = typeErrOrig;                    
                end
            end
            nodeIdx = parIdx;%update 
        else
            break;
        end
    end
    
    %propagate forward
    gapSize = 0;
    queue = treenodeIdMap( trackingMatrixGT(pos(kk),1) );
    typeErrOrig = typeErr( pos(kk) );
    while ( isempty(queue) == false )
        nodeIdx = queue(1);
        queue(1) = [];
        while(gapSize < 3)%we still consider errors to be correlated
            chIdx = childrenIdMap( nodeIdx, 1);
            if( childrenIdMap( nodeIdx, 2) > 0 )%add division if necessary
                queue = [queue childrenIdMap( nodeIdx, 2)];
            end
            if( chIdx > 0 )
                if( typeErr( chIdx ) < 0 )%this was not an error
                    gapSize = gapSize + 1;
                else
                    gapSize = 0;
                    if( typeErr( chIdx ) == 0 )
                        typeErr( chIdx ) = typeErrOrig;
                    end
                end
                nodeIdx = chIdx;%update
            else
                break;
            end
        end
    end
end
nAfter = sum( typeErr > 0 );

disp(['Number of predicted errors before time correlation= ' num2str(nOrig) ';After = ' num2str(nAfter)]);


%%
display(['Total of ' num2str(sum(numSegmentationTotal)) ' nodes in GT. Segmentation accuracy is ' num2str( sum(numSegmentationCorrect) / sum(numSegmentationTotal) ) '.Linkage accuracy is ' num2str( sum(numLinkageCorrect) / sum(numLinkageTotal) )]);



%cumulative error (to see how it evolves
qq = cumsum(numSegmentationCorrect)./cumsum(numSegmentationTotal);
qq2 = [1.0; cumsum(numLinkageCorrect(2:end))./cumsum(numLinkageTotal(2:end))];
figure;plot(timeT,[qq qq2]);
legend('Segmentation','Linkage (correct segmentation)');
title('Cumulative accuracy')

