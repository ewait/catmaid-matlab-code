%first execute pairwiseSegmentationLinakgeError.m to get matrix info

%for each leaf in the groudn truth lineage we trace back as far a spossible
%and see how distance grows between GT lineage and automatic

%trackingMatrixGT fields
%id, type, x, y, z, radius, parent_id, time, confidence, skeletong_id
function [pathLengthLineages, distanceLineages, nearestNeighborsDist] = errorDistanceDivergenceLineage(tEnd, projectName, basenameXMLGMM, trackingMatrixGT, trackingMatrixErrorCode)


%annotations can have gaps without confidence = 5 (I need whole skeleton)
%main parameters
if(nargin < 1)
    projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID
end

addpath ../dbConnection
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

tic;
%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points for each skeleton
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
trackingMatrix = zeros(size(trackingMatrixGT,1)*10, size(trackingMatrixGT,2) );
N = 0;

skeletonIdVec = unique( trackingMatrixGT(:,10) );
for ii = 1:length(skeletonIdVec)
    skeletonId = skeletonIdVec(ii);
    WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND skeleton_id = ' num2str(skeletonId)];
    trackingMatrixAux = retrieveLineageFilter( connDB, WHEREclause );
    
    if( isempty( trackingMatrixAux) == false )
        for kk = 1: 3
            trackingMatrixAux(:,kk+2) = scale(kk) * trackingMatrixAux(:,kk + 2) / stackRes(kk);
        end
        Naux = size(trackingMatrixAux,1);
        
        trackingMatrix(N+1:N+Naux,:) = trackingMatrixAux;
        N = N + Naux;
    end
    
end
trackingMatrix = trackingMatrix(1:N,:);

treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

rmpath ../dbConnection
toc;

NGT = size( trackingMatrixGT,1);
treenodeIdMapGT = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

%-------------------------------------------------------
%remove branches that are not curated for the most part
tic;
isDivision = zeros(N,1);
for kk = 1:N
    nodeIdPar = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, nodeIdPar ) == true )%we do not have parent in ground truth
        aux = treenodeIdMap( nodeIdPar );
        isDivision( aux ) = isDivision( aux ) + 1;
    end
end
isLeaf = (isDivision == 0 & trackingMatrix(:,7) >= 0);
isDivision = (isDivision > 1);

posL = find( isLeaf == true | isDivision == true);

erase = zeros(N,1);
eraseN = 0;
for kk = 1: length(posL)
    eraseAux = zeros(1000,1);
    eraseAuxN = 0;
    numErr = 0;
    numGT = 0;
    nodeId = posL(kk);
    parId = trackingMatrix( nodeId,7);
    endTM = trackingMatrix( nodeId,8);
    iniTM = endTM; %so length of branch is endTM - iniTM + 1
    while( isKey( treenodeIdMap, parId ) == true )
        eraseAuxN = eraseAuxN + 1;
        eraseAux(eraseAuxN) = nodeId;
        %check if this node was in the gt subset
        if( isKey( treenodeIdMapGT, trackingMatrix(nodeId,1) ) == true )
            numGT = numGT + 1;
            nodeIdGT = treenodeIdMapGT( trackingMatrix(nodeId,1) );
            if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdGT,7) ) == true && trackingMatrixErrorCode(nodeIdGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                numErr = numErr + 1;
            end
        end
        
        %update for next step
        par = treenodeIdMap( parId );
        if( isDivision( par ) == true )
            break;
        end
        nodeId = par;
        iniTM = trackingMatrix( par,8);
        parId = trackingMatrix( par,7);
        if( numGT == 0 )%do not start counting until the first curated poitn is hit
            endTM = iniTM;
        end
    end
    
    %only keept branches where the majority of cells have been curated
    branchL = endTM - iniTM + 1;
    if( numGT / branchL < 0.8 )
        erase(eraseN+1:eraseN+eraseAuxN) = eraseAux(1:eraseAuxN);
        eraseN = eraseN + eraseAuxN;
    end
end

erase = erase(1:eraseN);
trackingMatrix(erase,:) = [];
%redo mapping
N = size(trackingMatrix,1);
treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

toc;


%-------------------------------------------------------
%map children in order to traverse forward
childrenIdMap = -ones( size(trackingMatrix,1) , 2);%  treenodeIdMap ( trackingMatrix( childrenIdMap(i,1), 7 ) ) = i
for kk = 1:size(trackingMatrix,1)
    parId = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, parId ) == true )
        parIdx = treenodeIdMap( parId );
        if( childrenIdMap(parIdx,1) < 0 )
            childrenIdMap(parIdx,1) = kk;
        else
            childrenIdMap(parIdx,2) = kk;
        end
    end
end

%----------------------------------------------------------
%load all objects from solution
%%
tic;
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
endTM = max( trackingMatrixGT(:,8));
iniTM = min( trackingMatrixGT(:,8));
numTM = endTM - iniTM + 1;
objCell = cell(numTM,1);
for frame = iniTM:endTM
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];
    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        TGMMfilename
        warning 'XML solution does not exist for as long as GT. You probably need to track further in time for full results'
        objCell = objCell(1:frame -iniTM);
        break;
    end
    objCell{frame + 1} = readXMLmixtureGaussians(TGMMfilename );
    
end
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
toc;

%%
%------------------------------------------------------
%calculate nearest neighbors distribution so I can compare
tic;
nearestNeighborsDist = cell(length(objCell),1);
for frame =1:length(objCell)
    xyz = zeros( length(objCell{frame}),3);
    for ii = 1:size(xyz,1)
        xyz(ii,:) = objCell{frame}(ii).m .* scale;
    end
    [~,dist] = knnsearch(xyz,xyz,'k',2);
    nearestNeighborsDist{frame} = dist(:,2);
end
toc;
%--------------------------------------------------------
%we know that branches that are left have been mostly curated ( we mainly
%removed spurious branches). Now we can follow GT forwward
%%
tic;

%for frame = endTM:-1:iniTM
frame = tEnd;
pp = find( trackingMatrix(:,8) == frame );
nLineages = length(pp);

xyz = zeros( length(objCell{frame+1}),3);
for ii = 1:size(xyz,1)
    xyz(ii,:) = objCell{frame+1}(ii).m .* scale; 
end
xyz = xyz(:,[2 1 3]);%flipped coordinates with respect to CATMAID


distanceLineages = cell(nLineages,1);
pathLengthLineages = distanceLineages;
%for each point we trace back and calculate distance
for ii = 1:length(pp)
    
    %tStart = tic;
    distanceLineages{ii} = -ones( frame + 1, 1);
    pathLengthLineages{ii} = -ones( frame + 1, 1);
       
    
    %find the matching element in TGMM solution
    nodeIdx = pp(ii);
    xyzOld = trackingMatrix(nodeIdx, 3:5);
    [valNN, posNN] = min( sum(bsxfun(@minus, xyz, trackingMatrix(nodeIdx, 3:5)).^2, 2));
    distanceLineages{ii}(frame+1) = sqrt(valNN);
    pathLengthLineages{ii}(frame+1) = 0;
    
    parId = trackingMatrix(nodeIdx, 7);
    parIdTGMM = objCell{frame+1}(posNN).parent + 1;%C-indexing
    count = frame;
    %look backwards until we find a mistake or teh track ends
    while( isKey( treenodeIdMap, parId ) == true && parIdTGMM > 0)
        nodeIdx = treenodeIdMap( parId );
        xyzGT = trackingMatrix(nodeIdx, 3:5);
        xyzTGMM = objCell{count}(parIdTGMM).m .* scale;
        xyzTGMM = xyzTGMM([2 1 3]);%flipped coordinates with respect to CATMAID
        
        pathLengthLineages{ii}(count) = pathLengthLineages{ii}(count + 1) + norm(xyzOld-xyzGT);
        distanceLineages{ii}(count) = norm(xyzTGMM-xyzGT);
        %update variables
        parId = trackingMatrix( nodeIdx, 7 );
        parIdTGMM = objCell{count}(parIdTGMM).parent + 1;%C-indexing
        count = count - 1;
        xyzOld = xyzGT;
    end
    %disp(['Processed frame ' num2str(frame) ' in ' num2str(toc(tStart)) 'secs']);
end



close(connDB);

toc;