function [nodeId, nodeIntensity, nodeXYZpixels] = debugRandomSamplingGTcatmaid( TM )

%main parameters
projectName = '12-08-28 Pairwise Random Sampling GT' %project containing the ground truth in CATMAID
imgPatternFilename = 'G:/12-08-28/Results/TimeFused.Blending/Dme_E1_His2ARFP.TM?????_timeFused_blending/SPC0_CM0_CM1_CHN00_CHN01.fusedStack_?????.jp2'


%%
%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND location_t=' num2str(TM)];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

rmpath( pathAdd );

%%
%load image
imgFilename = recoverFilenameFromPattern(imgPatternFilename, TM);
[~, ~, ext] = fileparts(imgFilename) ;
if( strcmp(ext,'.jp2') == 1 )
    im = readJPEG2000stack(imgFilename, 8 );
else
    im = readTIFFstack(imgFilename );
end

%%
%check intensity for each node
N = size(trackingMatrixGT,1);
nodeId = trackingMatrixGT(:,1); 
nodeIntensity = zeros(N,1);
nodeXYZpixels = trackingMatrixGT(:,3:5);
for ii = 1:3
   nodeXYZpixels(:,ii) = nodeXYZpixels(:,ii)/stackRes(ii); 
end

for ii = 1:N
   xyz = round(trackingMatrixGT(ii,3:5) ./ stackRes);%image coordinate
   nodeIntensity(ii) = im(xyz(2),xyz(1),xyz(3));%xy flipped with respect to JP2 files
end

%%
close(connDB);
