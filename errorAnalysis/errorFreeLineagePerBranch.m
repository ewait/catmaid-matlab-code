%first call errFreeLineage.m to obtain branchCellErr
function branchErrFreeLength = errorFreeLineagePerBranch( branchInfo, branchCellErr)

N = size( branchInfo,1);
branchErrFreeLength = cell(N,1);
for kk = 1:N
   iniTM = branchInfo(kk,1);
   endTM = branchInfo(kk,2);
   
   tErr = branchCellErr{kk};
   if( branchInfo(kk,3) ~= length(tErr) )
       error 'Quantities should match'
   end
   
   qq = diff(unique([iniTM tErr endTM]));
   
   if( length(qq) > 1 )%otherwise it means there were no errors
       qq( qq < 3 ) = [];%we consider them part of the same mistake
       
       if( length(qq) > 1 )
           if( qq(1) > qq(end) )%to avoid boundary bias. For example a diff with TM [3 50] should only count 50 (the 3 is because it was stopped due to end of branch)
              qq(end) = [];
           else
               qq(1) = [];
           end
       end
   end
   
   if( isempty(qq) == true )
       branchErrFreeLength{kk} = diff(unique([iniTM tErr endTM]));%short branches where everything is <3
   else    
       branchErrFreeLength{kk} = qq;
   end
end



