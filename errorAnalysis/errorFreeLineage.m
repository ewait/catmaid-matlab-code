%first execute pairwiseSegmentationLinakgeError.m to get matrix info

%trackingMatrixGT fields
%id, type, x, y, z, radius, parent_id, time, confidence, skeletong_id
function [branchInfo, branchErrCell] = errorFreeLineage(projectName, trackingMatrixGT, trackingMatrixErrorCode)


%annotations can have gaps without confidence = 5 (I need whole skeleton)
%main parameters
if(nargin < 1)
    projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID    
end

addpath ../dbConnection
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

tic;
%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]   
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)]; 
end

%retrieve all points for each skeleton
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
trackingMatrix = zeros(size(trackingMatrixGT,1)*10, size(trackingMatrixGT,2) );
N = 0;

skeletonIdVec = unique( trackingMatrixGT(:,10) );
for ii = 1:length(skeletonIdVec)
    skeletonId = skeletonIdVec(ii);
    WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND skeleton_id = ' num2str(skeletonId)];
    trackingMatrixAux = retrieveLineageFilter( connDB, WHEREclause );
    
    for kk = 1: 3
        trackingMatrixAux(:,kk+2) = scale(kk) * trackingMatrixAux(:,kk + 2) / stackRes(kk);
    end
    Naux = size(trackingMatrixAux,1);
    
    trackingMatrix(N+1:N+Naux,:) = trackingMatrixAux;
    N = N + Naux;
end
trackingMatrix = trackingMatrix(1:N,:);

treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

rmpath ../dbConnection
toc;

NGT = size( trackingMatrixGT,1);
treenodeIdMapGT = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

%-------------------------------------------------------
%remove branches that are not curated for the most part
tic;
isDivision = zeros(N,1);
for kk = 1:N
    nodeIdPar = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, nodeIdPar ) == true )%we do not have parent in ground truth
        aux = treenodeIdMap( nodeIdPar );
        isDivision( aux ) = isDivision( aux ) + 1;
    end
end
isLeaf = (isDivision == 0 & trackingMatrix(:,7) >= 0);
isDivision = (isDivision > 1);

posL = find( isLeaf == true | isDivision == true);

erase = zeros(N,1);
eraseN = 0;
for kk = 1: length(posL)
    eraseAux = zeros(1000,1);
    eraseAuxN = 0;
    numErr = 0;
    numGT = 0;
    nodeId = posL(kk);
    parId = trackingMatrix( nodeId,7);
    endTM = trackingMatrix( nodeId,8);
    iniTM = endTM; %so length of branch is endTM - iniTM + 1
    while( isKey( treenodeIdMap, parId ) == true )
        eraseAuxN = eraseAuxN + 1;
        eraseAux(eraseAuxN) = nodeId;
        %check if this node was in the gt subset
        if( isKey( treenodeIdMapGT, trackingMatrix(nodeId,1) ) == true )
            numGT = numGT + 1;
            nodeIdGT = treenodeIdMapGT( trackingMatrix(nodeId,1) );
            if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdGT,7) ) == true && trackingMatrixErrorCode(nodeIdGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                numErr = numErr + 1;
            end
        end
        
        %update for next step
        par = treenodeIdMap( parId );
        if( isDivision( par ) == true )
            break;
        end
        nodeId = par;
        iniTM = trackingMatrix( par,8);
        parId = trackingMatrix( par,7);
        if( numGT == 0 )%do not start counting until the first curated poitn is hit
            endTM = iniTM;
        end
    end
    
    %only keept branches where the majority of cells have been curated
    branchL = endTM - iniTM + 1;
    if( numGT / branchL < 0.8 )
        erase(eraseN+1:eraseN+eraseAuxN) = eraseAux(1:eraseAuxN);
        eraseN = eraseN + eraseAuxN;
    end
end

erase = erase(1:eraseN);
trackingMatrix(erase,:) = [];
%redo mapping
N = size(trackingMatrix,1);
treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

toc;

%--------------------------------------------------------
%we know that branches that are left have been mostly curated ( we mainly
%removed suprious branches). Now we can do statistics
tic;

%find leaves and divisions (redo calculation)
isDivision = zeros(N,1);
for kk = 1:N
    nodeIdPar = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, nodeIdPar ) == true )%we do not have parent in ground truth
        aux = treenodeIdMap( nodeIdPar );
        isDivision( aux ) = isDivision( aux ) + 1;
    end
end
isLeaf = (isDivision == 0 & trackingMatrix(:,7) >= 0);
isDivision = (isDivision > 1);

posL = find( isLeaf == true | isDivision == true);
disp(['Found ' num2str(length(posL)) ' leaves']);


%calculate percentage of correct elements per branch
branchInfo = zeros(length(posL),3);%iniTM, endTM, numErrors
branchN = 0;
branchErrCell = cell(length(posL),1);%indiates the time (between iniTM-endTM) where errors where encoutered
tErr = zeros(1,500);
for kk = 1: length(posL)
    tErrN = 0;
    numErr = 0;
    numGT = 0;
    nodeId = posL(kk);
    parId = trackingMatrix( nodeId,7);
    endTM = trackingMatrix( nodeId,8);
    iniTM = endTM; %so length of branch is endTM - iniTM + 1
    while( isKey( treenodeIdMap, parId ) == true )
        %check if this node was in the gt subset
        if( isKey( treenodeIdMapGT, trackingMatrix(nodeId,1) ) == true )
            numGT = numGT + 1;
            nodeIdGT = treenodeIdMapGT( trackingMatrix(nodeId,1) );
            if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdGT,7) ) == true && trackingMatrixErrorCode(nodeIdGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                numErr = numErr + 1;
                tErrN = tErrN + 1;
                tErr( tErrN ) = trackingMatrix( nodeId,8);
            end
        end
        
        %update for next step
        par = treenodeIdMap( parId );
        if( isDivision( par ) == true )
            break;
        end
        nodeId = par;
        iniTM = trackingMatrix( par,8);
        parId = trackingMatrix( par,7);
        if( numGT == 0 )%do not start counting until the first curated poitn is hit
            endTM = iniTM;
        end
    end
    
    %only keept branches where the majority of cells have been curated
    branchL = endTM - iniTM + 1;
    if( numGT / branchL > 0.8 )
        branchN = branchN + 1;
        branchInfo(branchN,:) = [iniTM, endTM, numErr];
        branchErrCell{branchN} = tErr(tErrN:-1:1);%sorted in time
    end
end

branchErrCell = branchErrCell(1:branchN);
branchInfo = branchInfo(1:branchN,:);
toc;

close(connDB);