%adaption of the TRA metric from ISBI2013 challenge when we do not have
%comprenhensive GT

%Look at TGMM paper notebook from 09/18/2013 for more details

%wTRA = [w_NS, w_FN, w_FP, w_ED1, w_ED2, w_EA, w_EC]
%TRA = sum(w .* sum(errC))
%errC = number time points x [NS, FN, FP, ED1, ED2, EA, EC]
%numM = number of nodes in GT per time point
%numMhat = number of points in tracking result per time point
function [TRA, errC, numM, numMhat] = TRA_ISBI2013challenge_partialGT(projectName, basenameXMLGMM, wTRA)

if( length(wTRA) ~= 7 )
   error 'wTra must have length = 7' 
end

%fixed parameters
maxDistanceSegmentationError = 10.0; %max distance to consider that two points are the same between ground truth and automatic reconstruction


%main parameters
if(nargin < 1)
    projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID
    basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_6_6_21_28_43_dataset12-08-28_TM0-1000_neuroblastAnnotation_simpleLogicalRules_noStructuralLearning\XML_finalResult_lht\GMEMfinalResult_frame';
end


%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 '];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

treenodeIdMapGT = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);



%retrieve all points for each skeleton containing ground truth
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
trackingMatrix = zeros(size(trackingMatrixGT,1)*10, size(trackingMatrixGT,2) );
N = 0;

skeletonIdVec = unique( trackingMatrixGT(:,10) );
for ii = 1:length(skeletonIdVec)
    skeletonId = skeletonIdVec(ii);
    WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND skeleton_id = ' num2str(skeletonId)];
    trackingMatrixAux = retrieveLineageFilter( connDB, WHEREclause );
    
    if( isempty( trackingMatrixAux) == false )
        for kk = 1: 3
            trackingMatrixAux(:,kk+2) = scale(kk) * trackingMatrixAux(:,kk + 2) / stackRes(kk);
        end
        Naux = size(trackingMatrixAux,1);
        
        trackingMatrix(N+1:N+Naux,:) = trackingMatrixAux;
        N = N + Naux;
    end
    
end
trackingMatrix = trackingMatrix(1:N,:);

N = size( trackingMatrix,1);
treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);


rmpath( pathAdd );

%-------------------------------------------------------
%remove branches that are not curated for the most part
tic;
isDivision = zeros(N,1);
for kk = 1:N
    nodeIdPar = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, nodeIdPar ) == true )%we do not have parent in ground truth
        aux = treenodeIdMap( nodeIdPar );
        isDivision( aux ) = isDivision( aux ) + 1;
    end
end
isLeaf = (isDivision == 0 & trackingMatrix(:,7) >= 0);
isDivision = (isDivision > 1);

posL = find( isLeaf == true | isDivision == true);

erase = zeros(N,1);
eraseN = 0;
for kk = 1: length(posL)
    eraseAux = zeros(1000,1);
    eraseAuxN = 0;
    numGT = 0;
    nodeId = posL(kk);
    parId = trackingMatrix( nodeId,7);
    endTM = trackingMatrix( nodeId,8);
    iniTM = endTM; %so length of branch is endTM - iniTM + 1
    while( isKey( treenodeIdMap, parId ) == true )
        eraseAuxN = eraseAuxN + 1;
        eraseAux(eraseAuxN) = nodeId;
        %check if this node was in the gt subset
        if( isKey( treenodeIdMapGT, trackingMatrix(nodeId,1) ) == true )
            numGT = numGT + 1;           
        end
        
        %update for next step
        par = treenodeIdMap( parId );
        if( isDivision( par ) == true )
            break;
        end
        nodeId = par;
        iniTM = trackingMatrix( par,8);
        parId = trackingMatrix( par,7);
        if( numGT == 0 )%do not start counting until the first curated poitn is hit
            endTM = iniTM;
        end
    end
    
    %only keept branches where the majority of cells have been curated
    branchL = endTM - iniTM + 1;
    if( numGT / branchL < 0.8 )
        erase(eraseN+1:eraseN+eraseAuxN) = eraseAux(1:eraseAuxN);
        eraseN = eraseN + eraseAuxN;
    end
end

erase = erase(1:eraseN);
trackingMatrix(erase,:) = [];
%redo mapping
N = size(trackingMatrix,1);
treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

toc;


%----------------------------------------------------------
%load all objects from solution
%%
tic;
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
endTM = max( trackingMatrixGT(:,8));
iniTM = min( trackingMatrixGT(:,8));
numTM = endTM - iniTM + 1;
objCell = cell(numTM,1);
for frame = iniTM:endTM
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];
    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        TGMMfilename
        warning 'XML solution does not exist for as long as GT. You probably need to track further in time for full results'
        objCell = objCell(1:frame -iniTM);
        break;
    end
    objCell{frame + 1} = readXMLmixtureGaussians(TGMMfilename );
    
end
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
toc;


%-----------------------------------------------------------
%%
%find all possible matches for each GT node
matchGT2TGMM = cell(N,1);
distGT2TGMM = cell(N,1);

%inverse map
matchTGMM2GT = cell(endTM+1,1);
distTGMM2GT = cell(endTM+1,1);

for frame = iniTM:endTM
    GT = trackingMatrix(trackingMatrix(:,8) == frame, : );
    
    
    obj = objCell{frame+1};
    %parse xyz points from automatic tracking
    xyz = zeros(length(obj),3);
    for kk = 1: size(xyz,1)
        xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y        
        if( obj(kk).id ~= kk-1 )
            error 'GMM Index is not sequential!'
        end
    end
    for kk = 1:3
        xyz(:,kk) = xyz(:,kk) * scale(kk);
    end        
    
    %find close points to segmentation points
    [idx, dist] = rangesearch(xyz, GT(:,3:5), maxDistanceSegmentationError);%dist has the size of GT
    [idx2, dist2] = knnsearch(xyz, GT(:,3:5),'k',1);%dist has the size of GT. We need to guarantee at least one nearest neighbor per GT point
    
    for kk = 1: size(GT,1)
        if( isempty(idx{kk}) )%guarantee at least one nearest neighbor
            matchGT2TGMM{ treenodeIdMap(GT(kk,1)) } = idx2(kk);
            distGT2TGMM{ treenodeIdMap(GT(kk,1)) } = dist2(kk);
        else
            matchGT2TGMM{ treenodeIdMap(GT(kk,1)) } = idx{kk};
            distGT2TGMM{ treenodeIdMap(GT(kk,1)) } = dist{kk};
        end
    end
    
    %find inverse map
    [idx, dist] = knnsearch(GT(:,3:5), xyz, 'k', 1);%dist has the size of xyz
        
    matchTGMM2GT{ frame+1 } = idx;
    distTGMM2GT{ frame +1 } = dist;
    
    for kk = 1:size(xyz,1)
        matchTGMM2GT{ frame+1 }(kk) = treenodeIdMap( GT( idx(kk) , 1) );
    end
end

%--------------------------------------------------------------------
%%
%calculate TRA measures
%errC = [NS, FN, FP, ED1, ED2, EA, EC]

%note: ED1 is always zero since it is redundant. When removing a node from
%a graph you already remove the edge to the parent associated with it
errC = zeros( endTM + 1, length( wTRA ) );

numM = zeros(numTM,1);%number of points in groundtruth
numMhat = numM;
numMhatCell = cell(endTM+1,1);
TP = zeros( endTM + 1, 1);% auxiliary variable
for kk = 1:N
    if( trackingMatrix(kk,9) ~= 5 )%not annotated
        continue;
    end
    tt = trackingMatrix(kk,8) + 1;%Matlab indexing
    numM(tt) = numM(tt) + 1;
    numMhatCell{tt} = [ numMhatCell{tt} matchGT2TGMM{kk} ];%number of TGMM detections associated with this element
    
    
    if( distGT2TGMM{kk}(1) > maxDistanceSegmentationError )%FN: GT point was not detected
        errC(tt,2) =  errC(tt,2) + 1;                
    else
        %if we are here it means we have at least one good detection(the
        %nearest neighbor)
        TP( tt ) = TP( tt ) + 1;
        %FP: check if there was oversegmentation
        if( length(distGT2TGMM{kk}) > 1 )
            aa = distGT2TGMM{kk}(2:end) / distGT2TGMM{kk}(1); 
            errC(tt,3) =  errC(tt,3) + sum(aa < 5.0 );
        end
        %EA: check if edge is missing
        if( isKey( treenodeIdMap, trackingMatrix( kk, 7) ) == true )
            %check that parent also had a true positive detection
            parIdx = treenodeIdMap( trackingMatrix( kk, 7) );
            if( distGT2TGMM{ parIdx }(1) <= maxDistanceSegmentationError )
               parTGMM = objCell{tt}( matchGT2TGMM{kk}(1) ).parent + 1;
               if( matchGT2TGMM{ parIdx }(1) ~=  parTGMM)
                  errC(tt,6) = errC(tt,6) + 1; 
               end
            end
        end
    end        
end

%compute numMhat
for kk = 1:length(numMhatCell)
   numMhat(kk) = length(unique(( numMhatCell{kk} ))); 
   
   %NS: check if there was undersegmentation (one TGMM point assigned to two different GT point)
   errC(kk,1) = length(( numMhatCell{kk} )) - numMhat(kk);
end

%compute number of edges in TGMM that need to be removed (ED2)
numUnmatched = zeros(length(numMhatCell),1);
numUnmatchedTest = zeros(length(numMhatCell),1);
for tt = 1:length(numMhatCell)
   numMhatCell{tt} = unique( numMhatCell{tt} ); 
   
   for ii = 1: length( numMhatCell{tt} )
      %find edge in TGMM
      idx = numMhatCell{tt}(ii);
      %find if this object was matched
      if( distTGMM2GT{tt}(idx) > maxDistanceSegmentationError )
          if( matchGT2TGMM{ matchTGMM2GT{tt}(idx) }(1) ~= idx )
              numUnmatchedTest(tt) = numUnmatchedTest(tt) + 1;
             %disp 'Warning: Object was not matched!' 
             numUnmatched(tt) = numUnmatched(tt) + 1;
             continue;
          end
      end
      obj = objCell{tt}( idx );
      idxPar = obj.parent + 1;
      if( idxPar > 0 )
          %objPar = objCell{tt-1}(idxPar);
          %find if this object was matched
          if( distTGMM2GT{tt-1}(idxPar) > maxDistanceSegmentationError )
              numUnmatchedTest(tt) = numUnmatchedTest(tt) + 1;
              if( matchGT2TGMM{ matchTGMM2GT{tt-1}(idxPar) }(1) ~= idxPar )
                  %disp 'Warning: Object parent was not matched!'
                  numUnmatched(tt) = numUnmatched(tt) + 1;
                  continue;
              end
          end
          %find if edge exists in GT          
          if( isKey( treenodeIdMap, trackingMatrix( matchTGMM2GT{tt}(idx), 7) ) == false )
              errC(tt,5) = errC(tt,5) + 1;
          else
              if( treenodeIdMap (trackingMatrix( matchTGMM2GT{tt}(idx), 7) ) ~= matchTGMM2GT{tt-1}(idxPar))
                  errC(tt,5) = errC(tt,5) + 1;
              end
          end
      end                  
   end
end
%---------------------------------------------------------------------
%%
TRA = sum(wTRA .* sum(errC));
close(connDB);

