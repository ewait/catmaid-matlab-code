%estimates precision vs curation time if we only look at a certain local
%spatio-temporal neighborhood around critical lineaging points

%numKNN and deltaTths decide the size of the local spatio temporal
%threshold

%CODE has been adapted from pairwiseSegmentationLinkageError.m
function [curationTime, prctErrCurated, numGTpoints, numErrTotal, numCheckedPts, numErrCorrected] = estimateCurationTime(numKNN, deltaTthr, projectName, basenameXMLGMM)

maxDistanceSegmentationError = 10.0 %max distance to consider that two points are the same between ground truth and automatic reconstruction
maxRestTimeBetweenProgress = 120 %(in secs) Dan curates edits one data point every 2.5secs in avg->120 secs is plenty to consider he took a break


%%
%retrieve groundtruth
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 '];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

numGTpoints = size(trackingMatrixGT,1);

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

treenodeIdMap = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

%rmpath( pathAdd );

%%
%retrieve editing time for each point to be able to calculate curation time
disp 'UNFORTUNATELY edition_time IS SAVED IN BATCHES, SO IT DOES NOT CONTIAN PRECISE TIMING INFORMATION TO ESTIMATE CURATION TIME OF EACH INDIVIDUAL DATA POINT->WE USE avg time FROM FIG.5 FOR DAN'
avgEditTime = 2.5864 %data points / secs

%{
clause =['SELECT id,edition_time FROM treenode WHERE confidence=5 AND project_id=' num2str(projectId) ' ORDER BY edition_time ASC'];

curs4 = exec(connDB,clause);
setdbprefs('DataReturnFormat','structure');%location_t cannot be retrieved as numeric
curs4 = fetch( curs4 );

progress = datenum( curs4.Data.edition_time,'yyyy-mm-dd HH:MM:SS.FFF') * 1e5;%in secs
progressIdMap =  containers.Map( curs4.Data.id,[1:length(progress)]);
close(curs4);
%}

%%
%code adapted from pairwiseSegmentationLinkageError.m
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
minT = min( trackingMatrixGT( : , 8 ) );
maxT = max( trackingMatrixGT( : , 8 ) );

typeErr = -ones( size(trackingMatrixGT,1), 1);%-1->indicates no error
numCheckedPts = [];

%savex xyz from TGMM in order to calculate KNN later on
xyzCell = cell(maxT+1,1);
for frame = minT : maxT
    
    
    %eleemnts for current time point
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        break
    end
    obj = readXMLmixtureGaussians(TGMMfilename );
    GT = trackingMatrixGT(trackingMatrixGT(:,8) == frame, : );
    
    
    %parse xyz points from automatic tracking
    xyz = zeros(length(obj),3);
    parId = zeros(length(obj),1);
    score = zeros(length(obj),1);
    for kk = 1: size(xyz,1)
        xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
        parId(kk) = obj(kk).parent + 1;%matlab index
        score(kk) = obj(kk).splitScore;
        if( obj(kk).id ~= kk-1 )
            error 'GMM Index is not sequential!'
        end
    end
    for kk = 1:3
        xyz(:,kk) = xyz(:,kk) * scale(kk);
    end
    xyzCell{frame+1} = createns(xyz);%kd-tree to query later
    
    
    %count segmentation errors
    [idx, dist] = knnsearch(xyz, GT(:,3:5),'k',2);%dist has the size of GT
    if( numKNN > 0 )
        [idxErrType, ~] = knnsearch(xyz, GT(:,3:5),'k',numKNN);%to calculate types of errors
    else
        idxErrType = [];
    end
    pos = find( dist(:,1) < maxDistanceSegmentationError & (dist(:,2)./ dist(:,1)) > 1.5 );
    
    %save info on errors
    posN = [1:size(idx,1)];
    posN(pos) = [];        
    typeErrAux = zeros(length(posN),1);%0->unknown; 1->cell death; 2->cell division; 3->large displacement
    numCheckedPtsAux = -ones(100000,1);
    numCheckedPtsAuxN = 0;
    
    %check if errors havea nearby cell division or death or large
    %displacements
    if( frame > minT )
        %calculate displacement distribution to find out large
        %displacements
        GTold = trackingMatrixGT(trackingMatrixGT(:,8) == frame-1, : );
        [~,dispD] = knnsearch(GT(:,3:5), GTold(:,3:5),'k',1);
        thrD = median(dispD) + 4 *  mad(dispD,1) * 1.4826;%median absolute deviation
        if( numKNN > 0 )
            [idxErrTypeDeath, ~] = knnsearch(xyzOld, GT(:,3:5),'k',numKNN);%to calculate errors based on death
        else
            idxErrTypeDeath = [];
        end
        %{
        %calculate global stats on TGMM on how many time points we would have to check
        %cell death
        auxParId = parId(parId>0);
        aux = size(xyzOld,1) - length(unique(auxParId));
        numTotalCheckedpts( frame ) = numTotalCheckedpts( frame ) + aux;
        %cell division        
        aux = length(auxParId) - length(unique(auxParId));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + aux;
        %large displacement                
        aux = sqrt(sum( ((xyz( parId > 0, : ) - xyzOld( auxParId , :)) ).^2,2));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + sum(aux > thrD);
        %}
        
        for kk = 1:length(posN)
            
            pp = posN(kk);
            %check if neighbors (in time and space) have a division
            %TODO: expand time search to +-2 time points (right now only to
            %-1)
            
            %check if neighbors in previous time point died            
            for aa = 1:size(idxErrTypeDeath,2)
                auxParId = idxErrTypeDeath( pp, aa);%this is ID in xyzOld
                if( sum( parId == auxParId ) == 0 )
                    typeErrAux(kk) = 1;%cell death
                    numCheckedPtsAuxN = numCheckedPtsAuxN + 1;%we would check this point in TGMM
                    numCheckedPtsAux(numCheckedPtsAuxN) = (frame-1) * 50000 + auxParId;%so it is unique per time point
                    break;%if we find it, we do not need to check any further
                end                
            end                        
            
            if( typeErrAux(kk) == 0 )%not assigned yet
                for aa = 1:size(idxErrType,2)                    
                    %check cell division
                    auxParId = idxErrType( pp, aa);                          
                    if (sum( parId == parId( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        numCheckedPtsAuxN = numCheckedPtsAuxN + 1;
                        numCheckedPtsAux(numCheckedPtsAuxN) = frame * 50000 + auxParId;
                        break;%if we find it, we do not need to check any further
                    end
                    %check if it is large displacement
                    if( parId( auxParId ) > 0 )
                        if( norm (xyz( auxParId ,: ) - xyzOld( parId( auxParId ), :)) > thrD )
                            numCheckedPtsAuxN = numCheckedPtsAuxN + 1;
                            numCheckedPtsAux(numCheckedPtsAuxN) = frame * 50000 + auxParId;
                            typeErrAux(kk) = 3;%large displacement
                             break;%if we find it, we do not need to check any further
                        end
                    end
                    
                    %previous time point                    
                    auxParId = parId( idxErrType( pp, aa) );                    
                    if (sum( parIdOld == parIdOld( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        numCheckedPtsAuxN = numCheckedPtsAuxN + 1;
                        numCheckedPtsAux(numCheckedPtsAuxN) = (frame-1) * 50000 + auxParId;
                        break;
                    end
                    
                end
            end                                                
        end
    end
    
    for aa = 1:length(posN)
       typeErr( treenodeIdMap( GT(posN(aa),1))) = typeErrAux(aa);  
    end
    numCheckedPts = [numCheckedPts; unique(numCheckedPtsAux(1:numCheckedPtsAuxN))];
    
    %update values    
    xyzOld = xyz;
    parIdOld = parId;    
end
%rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'

%%
%-------------------------------------------------------
%map children in order to traverse forward
childrenIdMap = -ones( size(trackingMatrixGT,1) , 2);%  treenodeIdMap ( trackingMatrixGT( childrenIdMap(i,1), 7 ) ) = i
for kk = 1:size(trackingMatrixGT,1)
    parId = trackingMatrixGT(kk,7);
    if( isKey( treenodeIdMap, parId ) == true )
        parIdx = treenodeIdMap( parId );
        if( childrenIdMap(parIdx,1) < 0 )
            childrenIdMap(parIdx,1) = kk;
        else
            childrenIdMap(parIdx,2) = kk;
        end
    end
end
%%
%-----------------correlate marked errors in time(since they tend to be correlated---------------------------
numCheckedPtsAux = -ones(100000,1);
numCheckedPtsAuxN = 0;
pos = find( typeErr > 0 );
nOrig = length(pos);
for kk = 1:length(pos)
    %propagate backwards
    gapSize = 0;
    nodeIdx = treenodeIdMap( trackingMatrixGT(pos(kk),1) );
    typeErrOrig = typeErr( pos(kk) );
    tOrig = trackingMatrixGT(nodeIdx,8);
    tt = tOrig;
    while(gapSize < 3 && abs(tOrig-tt)<=deltaTthr)%we still consider errors to be correlated
        parId = trackingMatrixGT( nodeIdx, 7);
        if( isKey(treenodeIdMap,parId) == true )
            parIdx = treenodeIdMap( parId );
            
            idx = knnsearch(xyzCell{tt+1},trackingMatrixGT( nodeIdx, 3:5),'k',1);%because this is correlated in time, I only have to curate one extra data point
            for aa = 1:length(idx)
                numCheckedPtsAuxN = numCheckedPtsAuxN + 1;
                numCheckedPtsAux(numCheckedPtsAuxN) = tt * 50000 + idx(aa);
            end
            
            if( typeErr( parIdx ) < 0 )%this was not an error
                gapSize = gapSize + 1;
            else
                gapSize = 0;
                if( typeErr( parIdx ) == 0 )
                    typeErr( parIdx ) = typeErrOrig;                    
                end
            end
            nodeIdx = parIdx;%update 
            tt = trackingMatrixGT(nodeIdx,8);
        else
            break;
        end
    end
    
    %propagate forward
    gapSize = 0;
    queue = treenodeIdMap( trackingMatrixGT(pos(kk),1) );
    typeErrOrig = typeErr( pos(kk) );
    tOrig = trackingMatrixGT(pos(kk),8);
    while ( isempty(queue) == false )
        nodeIdx = queue(1);
        tt = trackingMatrixGT(nodeIdx,8);
        queue(1) = [];
        while(gapSize < 3 && abs(tOrig-tt)<=deltaTthr)%we still consider errors to be correlated
            chIdx = childrenIdMap( nodeIdx, 1);
            if( childrenIdMap( nodeIdx, 2) > 0 )%add division if necessary
                queue = [queue childrenIdMap( nodeIdx, 2)];
            end
            if( chIdx > 0 )
                idx = knnsearch(xyzCell{tt+1},trackingMatrixGT( nodeIdx, 3:5),'k',1);%because this is correlated in time, I only have to curate one extra data point
                for aa = 1:length(idx)
                    numCheckedPtsAuxN = numCheckedPtsAuxN + 1;
                    numCheckedPtsAux(numCheckedPtsAuxN) = tt * 50000 + idx(aa);
                end
                if( typeErr( chIdx ) < 0 )%this was not an error
                    gapSize = gapSize + 1;
                else
                    gapSize = 0;
                    if( typeErr( chIdx ) == 0 )
                        typeErr( chIdx ) = typeErrOrig;
                    end
                end
                nodeIdx = chIdx;%update
                tt = trackingMatrixGT(nodeIdx,8);
            else
                break;
            end
        end
    end
end
nAfter = sum( typeErr > 0 );
numCheckedPts = [numCheckedPts; unique(numCheckedPtsAux(1:numCheckedPtsAuxN))];
disp(['Number of predicted errors before time correlation= ' num2str(nOrig) ';After = ' num2str(nAfter)]);

%%
numErrTotal = sum(typeErr>=0);
numErrCorrected = sum(typeErr > 0);
prctErrCurated = sum(typeErr > 0 ) / sum(typeErr>=0);
curationTime = length(unique((numCheckedPts))) * avgEditTime;

%%
close(connDB);
