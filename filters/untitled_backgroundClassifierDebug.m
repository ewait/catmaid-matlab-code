%%
kk = 15000; 
ww = 20;

treenodeIdMap = containers.Map(trackingMatrix(:,1),[1:size(trackingMatrix,1)]);
[val,pos] = min( sum( abs( bsxfun(@minus,qq.trackingMatrix(erase(kk),[3:5 8]), trackingMatrix(:,[3:5 8]))),2));
if( abs(val) > 1e-5 )
    error 'Match not found!!!'
end
disp(['Selected TM = ' num2str(trackingMatrix(pos,8)) '. NodeIdx=' num2str(trackingMatrix(pos,1))]);

%track element forward and backward
numSteps = 0;
parId = trackingMatrix(pos,1);
nodeIdx = pos;
chIdx = find( trackingMatrix(:,7) == parId );

while( length(chIdx)==1 && numSteps < ww )
    nodeIdx = chIdx;
    parId = trackingMatrix(chIdx,1);
    chIdx = find( trackingMatrix(:,7) == parId );
    numSteps = numSteps + 1;
end

probBackg = [nodeIdx trackingMatrix(nodeIdx,6)];
parIdx = treenodeIdMap( trackingMatrix(nodeIdx,7) );

while( parIdx > 0 && numSteps > -ww )
   nodeIdx = parIdx; 
    
   probBackg = [probBackg; nodeIdx  trackingMatrix(nodeIdx,6)];
   numSteps = numSteps - 1;
   
   if( isKey( treenodeIdMap,trackingMatrix(nodeIdx,7)) == true )
       parIdx = treenodeIdMap( trackingMatrix(nodeIdx,7) );
   else
       parIdx = -1;
   end
end

probBackg = flipud( probBackg);

[trackingMatrix(probBackg(:,1),8)  probBackg(:,2)]

%[trackingMatrix(probBackg(:,1),1) trackingMatrix(probBackg(:,1),7)]