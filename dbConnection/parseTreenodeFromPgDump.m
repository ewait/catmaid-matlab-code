%pgdump creates .dat files (space seprated txt files) with all the content
%from the table. So I can just extract those files from the nightly tgz
%backups and retireve information
function treenode = parseTreenodeFromPgDump(datFilename)


%nodeId, user_id, creation_time, edition_time, project_id, x, y, z,reviewer_id, reviewer_time, editor_id, parent_id, radius, confidence, skeleton_id, location_t, location_c


%calculate number of lines for preallocation
[status, result] = system( ['wc -l ', datFilename] );
pp = find(isspace(result));
numLines = str2num( result(1:pp(1)) );

treenode = zeros(numLines,17);


fid = fopen(datFilename);
N = 0;
Nerr = 0;
while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    
    %empty lines
    if( isempty(tline) || isnan(str2double(tline(1))) )
        continue;
    end
    
    tline = strrep(tline, '\N', 'NaN');
    
    N = N+1;
    pp = [find(isspace(tline))];
    
    
    try
        treenode(N,[1 2]) = str2num(tline(1:pp(2)));
        
        qq = tline(pp(2)+1:pp(4)-4);%remove the time zone (-04)
        if ( isempty(strfind(qq,'.')) ) %we have a number formatted 2013-06-03 16:27:52 (we have to add milliseconds precision)
            qq = [qq '.00000'];                        
        end
        treenode(N,3) = datenum( qq,'yyyy-mm-dd HH:MM:SS.FFF');
        
        qq = tline(pp(4)+1:pp(6)-4);%remove the time zone (-04)
        if ( isempty(strfind(qq,'.')) ) %we have a number formatted 2013-06-03 16:27:52 (we have to add milliseconds precision)
            qq = [qq '.00000'];                        
        end
        treenode(N,4) = datenum( qq,'yyyy-mm-dd HH:MM:SS.FFF');
        
        treenode(N,5) = str2double( tline(pp(6):pp(7)));
        treenode(N,6:8) = sscanf(tline(pp(7)+1:pp(8)-1),'(%f,%f,%f)')';
        
        treenode(N,9) = str2double( tline(pp(8):pp(9)));%reviewer_id
        
        if( strcmp(tline(pp(9)+1:pp(10)-1),'NaN') == 1)
            treenode(N,10) = NaN;
            pNext = 10;
        else
            qq = tline(pp(9)+1:pp(11)-4);%remove the time zone (-04)
            if ( isempty(strfind(qq,'.')) ) %we have a number formatted 2013-06-03 16:27:52 (we have to add milliseconds precision)
                qq = [qq '.00000'];
            end
            treenode(N,10) = datenum( qq,'yyyy-mm-dd HH:MM:SS.FFF');%revieer_time
            pNext = 11;
        end
        
        treenode(N,11:end) = str2num( tline(pp(pNext):end));
        
    catch err
        disp 'WARNING: error parsing the following line'
        tline
        N = N - 1;
        Nerr = Nerr + 1;
    end
end

fclose(fid);

Nerr
