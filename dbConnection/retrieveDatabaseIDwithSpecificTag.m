function [nodeId, skeletonId] = retrieveDatabaseIDwithSpecificTag(projectName, tag)


%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%retireves nodes with specific tag



connDB = connectToDatabase(databasename, databaseURL, username, password);

[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);


%find out tag id
setdbprefs('DataReturnFormat','numeric');%restore default value
curs = exec(connDB,['SELECT id FROM class_instance WHERE  project_id = ' '''' num2str(projectId) '''' ' AND name=''' tag '''']);
curs = fetch(curs);

if( length( curs.Data ) ~= 1 )
    error 'Tag does not exist'
end

tagId = curs.Data(1);

%find all the node ids with that tag
curs = exec(connDB,['SELECT treenode_id FROM treenode_class_instance WHERE  project_id = ' '''' num2str(projectId) '''' ' AND class_instance_id=''' num2str(tagId) '''']);
curs = fetch(curs);

treenodeId = curs.Data;


%we follow the SWC convention (more or less, since we need time )
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
aux = sprintf('%ld,',int64(treenodeId));
aux = aux(1:end-1);
curs = exec(connDB,['SELECT id, skeleton_id  FROM treenode WHERE id IN (' aux ') ORDER BY location_t']);
setdbprefs('DataReturnFormat','numeric');%location_t cannot be retrieved as numeric
curs = fetch(curs);


nodeId = curs.Data(:,1);
skeletonId = curs.Data(:,2);


close(curs);
close(connDB);
