%retireves nodes with specific tag

%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();


projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'

tag = 'NEUROBLAST'


connDB = connectToDatabase(databasename, databaseURL, username, password);

[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);


%find out tag id
setdbprefs('DataReturnFormat','numeric');%restore default value
curs = exec(connDB,['SELECT id FROM class_instance WHERE  project_id = ' '''' num2str(projectId) '''' ' AND name=''' tag '''']);
curs = fetch(curs);

if( length( curs.Data ) ~= 1 )
    error 'Tag does not exist'
end

tagId = curs.Data(1);

%find all the node ids with that tag
curs = exec(connDB,['SELECT treenode_id FROM treenode_class_instance WHERE  project_id = ' '''' num2str(projectId) '''' ' AND class_instance_id=''' num2str(tagId) '''']);
curs = fetch(curs);

treenodeId = curs.Data;


%we follow the SWC convention (more or less, since we need time )
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
aux = sprintf('%ld,',int64(treenodeId));
aux = aux(1:end-1);
curs = exec(connDB,['SELECT id, location, parent_id, radius, confidence, location_t, location_c, skeleton_id  FROM treenode WHERE id IN (' aux ') ORDER BY location_t']);
setdbprefs('DataReturnFormat','structure');%location_t cannot be retrieved as numeric
curs = fetch(curs);



trackingMatrix = zeros( length(curs.Data.id ), 10 );

trackingMatrix(:, 1) = curs.Data.id;
trackingMatrix(:, 6) = curs.Data.radius;
trackingMatrix(:, 7) = curs.Data.parent_id;
trackingMatrix( isnan(trackingMatrix(:, 7)) , 7) = -1;
trackingMatrix(:, 8) = curs.Data.location_t;
trackingMatrix(:, 9) = curs.Data.confidence;
trackingMatrix(:, 10) = curs.Data.skeleton_id;

qq = curs.Data.location;
aux = zeros(size(trackingMatrix,1),3);
disp 'Start parsing double3d'
for kk = 1:size(trackingMatrix, 1) %for some reason the code crashes with parfor    
    %trackingMatrix(kk, 3:5) = sscanf(char(qq{kk}),'(%f,%f,%f)')';%I cannot do this inside a parfor
    aux(kk,:) = sscanf(char(qq{kk}),'(%f,%f,%f)')';
end
trackingMatrix(:, 3:5 ) = aux;

setdbprefs('DataReturnFormat','cellarray');%restore default value

close(curs);
close(connDB);